<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>CSS Mobile Media Queries</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Base css -->
		<link rel="stylesheet" type="text/css"href="css/base.css" />
		<!-- True if greater than or equal to 320px -->
		<link rel="stylesheet" type="text/css" media="only screen and (min-width: 320px)" href="css/screen-min-320.css" />
		<!-- True if greater than or equal to 768px -->
		<link rel="stylesheet" type="text/css" media="only screen and (min-width: 768px)" href="css/screen-min-768.css" />
		<!-- True if greater than or equal to 1024px -->
		<link rel="stylesheet" type="text/css" media="only screen and (min-width: 1024px)" href="css/screen-min-1024.css" />
		 <!--Using jQuery and jQuery UI for display effects-->
		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

    <!--Using the hamburger menu display code-->
    <script src="js/hamburger.js"></script>
    </head>
    <body>

		<!--div class="navigation">
			<ul>
				<li><a href="" title="Home">Home</a>
				<li><a href="" title="About">About</a>
				<li><a href="" title="Contact">Contact</a>
			</ul>
		</div-->

<!--This wrapping container is used to get the width of the whole content-->
<div id="container">

    <!--The Hamburger Button in the Header-->
    <header>
        <div id="hamburger">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </header>

    <!--The mobile navigation Markup hidden via css-->
    <nav>
        <ul>
            <li><a href="#">menuitem 1</a></li>
            <li><a href="#">menuitem 2</a></li>
            <li><a href="#">menuitem 3</a></li>
            <li><a href="#">menuitem 4</a></li>
            <li><a href="#">menuitem 5</a></li>
            <li><a href="#">menuitem 6</a></li>
        </ul>
    </nav>

    <!--The Layer that will be layed over the content
    so that the content is unclickable while menu is shown-->
    <div id="contentLayer"></div>

    <!--The content of the site-->
    <div id="content">
        <h1>The Hamburger</h1>

        <h2>A Mobile Menu Template</h2>

        <p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
            eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
            voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
            Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
            sit amet.
        </p>
    </div>
</div>

        <!--div class="header-container">
            <header class="wrapper clearfix">
                <h1 class="title">h1.title</h1>
                <nav>
                    <ul>
                        <li><a href="#">nav ul li a</a></li>
                        <li><a href="#">nav ul li a</a></li>
                        <li><a href="#">nav ul li a</a></li>
                    </ul>
                </nav>
            </header>
        </div-->

        <div class="main-container">
            <div class="main wrapper clearfix">

                <article>
                    <header>
                        <h1>article header h1</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec.</p>
                    </header>
                    <section>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor. Etiam ullamcorper lorem dapibus velit suscipit ultrices. Proin in est sed erat facilisis pharetra.</p>
                    </section>
                    <section>
                        <h2>article section h2</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor. Etiam ullamcorper lorem dapibus velit suscipit ultrices. Proin in est sed erat facilisis pharetra.</p>
                    </section>
                    <footer>
                        <h3>article footer h3</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor.</p>
                    </footer>
                </article>

                <aside>
                    <h3>aside</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sodales urna non odio egestas tempor. Nunc vel vehicula ante. Etiam bibendum iaculis libero, eget molestie nisl pharetra in. In semper consequat est, eu porta velit mollis nec. Curabitur posuere enim eget turpis feugiat tempor. Etiam ullamcorper lorem dapibus velit suscipit ultrices.</p>
                </aside>

            </div> <!-- #main -->
        </div> <!-- #main-container -->

        <div class="footer-container">
            <footer class="wrapper">
                <h3>footer</h3>
            </footer>
        </div>

        <!--script src="js/main.js"></script-->

    </body>
</html>
